<?php include_once('header.php');  ?>
<section id="main-slider" class="mb-9">
  <div class="cont-fluid">
    <div class="row no-gutters justify-content-center">
      <div class="col-lg-3 black">
        <div class="t-1">
          <div class="tc-1">
            <img src="assets/images/11oct-black.png">
          </div>
        </div>
      </div>
      <div class="col-lg-9 black">
        <video width="100%"  height="100%" autoplay>
          <source src="assets/video/first.mp4" type="video/mp4">
          <source src="assets/video/first.mp4" type="video/ogg">
        Your browser does not support the video tag.
        </video>
      </div>
    </div>
  </div>
</section>
<!--  -->
<section class="big-slider"></section>
<section class="big-slider2">
  <!-- start section portfolio -->
  <div id="portfolio" class="text-center block">

    <div class="container">
        <div class="text-center feature-title">
          <h1 class="main-title">Rug Categories</h1>
        </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <div class="portfolio-list">
                  <ul class="nav list-unstyled" id="portfolio-flters">
                        <div class="row justify-content-center">
                          <div class="col-lg-3 col-md-3 col-sm-3 col-4">
                              <figure class="blue-hover">
                                <img src="assets/images/7.png">
                                <figcaption>
                                  <h3>Category Name</h3>
                                    <li class="filter" data-filter=".branding"><a class="blue-hover-btn"><button class="btn custom-btn2">View More</button></a></li>
                              </figcaption><i class="ion-ios-home-outline"></i>
                              </figure><!--  -->
                          </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-4">
                      <figure class="blue-hover">
                        <img src="assets/images/7.png">
                            <figcaption>
                              <h3>Category Name</h3>
                                <li class="filter" data-filter=".mockups"><a class="blue-hover-btn"><button class="btn custom-btn2">View More</button></a></li>
                          </figcaption><i class="ion-ios-home-outline"></i>
                      </figure><!--  -->
                    </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-4">
                          <figure class="blue-hover">
                            <img src="assets/images/7.png">
                              <figcaption>
                                <h3>Category Name</h3>
                                  <li class="filter" data-filter=".photography"><a class="blue-hover-btn"><button class="btn custom-btn2">View More</button></a></li>
                            </figcaption><i class="ion-ios-home-outline"></i>
                          </figure><!--  -->
                        </div>
                    </div>
                  </ul>
          </div>

          <div class="portfolio-container">

            <div class="col-lg-4 col-md-6 col-6 portfolio-thumbnail all branding uikits webdesign">
              <a class="popup-img" href="images/portfolio/1.jpg">
                  <div class="custom-card ecommerce">
                           <section class="cards cf">
                           <article class="fancy-card one">
                             <img src="assets/images/1.jpg">
                             <div class="bg-overlay"></div>
                             <div class="v-border"></div>
                             <div class="h-border"></div>
                             <div class="content">
                               <div class="secondary">
                                 <a href="#" class="btn custom-btn2" >View More</a>
                               </div>
                             </div>
                           </article>
                         </section>    
                          <div class="custom-card-body ecommerce-body"><!--  -->
                            <h3 class="product-title">Floral Patterns</h3>
                            <span class="category-name"> Category Name</span>
                            <div class="btn-price">
                              <button class="btn custom-btn"><i class="fas fa-cart-arrow-down"></i> Add to cart </button>
                              <span class="price">$123.89</span>
                            </div>
                          </div>
                        </div><!-- custom-card ends  -->
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-6 portfolio-thumbnail all mockups uikits photography">
              <a class="popup-img" href="images/portfolio/2.jpg">
                  <div class="custom-card ecommerce">
                           <section class="cards cf">
                           <article class="fancy-card one">
                             <img src="assets/images/1.jpg">
                             <div class="bg-overlay"></div>
                             <div class="v-border"></div>
                             <div class="h-border"></div>
                             <div class="content">
                               <div class="secondary">
                                 <a href="#" class="btn custom-btn2" >View More</a>
                               </div>
                             </div>
                           </article>
                         </section>    
                          <div class="custom-card-body ecommerce-body"><!--  -->
                            <h3 class="product-title">Floral Patterns</h3>
                            <span class="category-name"> Category Name</span>
                            <div class="btn-price">
                              <button class="btn custom-btn"><i class="fas fa-cart-arrow-down"></i> Add to cart </button>
                              <span class="price">$123.89</span>
                            </div>
                          </div>
                        </div><!-- custom-card ends  -->
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-6 portfolio-thumbnail all branding webdesig photographyn">
              <a class="popup-img" href="images/portfolio/3.jpg">
                  <div class="custom-card ecommerce">
                           <section class="cards cf">
                           <article class="fancy-card one">
                             <img src="assets/images/1.jpg">
                             <div class="bg-overlay"></div>
                             <div class="v-border"></div>
                             <div class="h-border"></div>
                             <div class="content">
                               <div class="secondary">
                                 <a href="#" class="btn custom-btn2" >View More</a>
                               </div>
                             </div>
                           </article>
                         </section>    
                          <div class="custom-card-body ecommerce-body"><!--  -->
                            <h3 class="product-title">Floral Patterns</h3>
                            <span class="category-name"> Category Name</span>
                            <div class="btn-price">
                              <button class="btn custom-btn"><i class="fas fa-cart-arrow-down"></i> Add to cart </button>
                              <span class="price">$123.89</span>
                            </div>
                          </div>
                        </div><!-- custom-card ends  -->
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-6 portfolio-thumbnail all mockups webdesign photography">
              <a class="popup-img" href="images/portfolio/4.jpg">
                  <div class="custom-card ecommerce">
                           <section class="cards cf">
                           <article class="fancy-card one">
                             <img src="assets/images/1.jpg">
                             <div class="bg-overlay"></div>
                             <div class="v-border"></div>
                             <div class="h-border"></div>
                             <div class="content">
                               <div class="secondary">
                                 <a href="#" class="btn custom-btn2" >View More</a>
                               </div>
                             </div>
                           </article>
                         </section>    
                          <div class="custom-card-body ecommerce-body"><!--  -->
                            <h3 class="product-title">Floral Patterns</h3>
                            <span class="category-name"> Category Name</span>
                            <div class="btn-price">
                              <button class="btn custom-btn"><i class="fas fa-cart-arrow-down"></i> Add to cart </button>
                              <span class="price">$123.89</span>
                            </div>
                          </div>
                        </div><!-- custom-card ends  -->
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-6 portfolio-thumbnail all branding uikits photography">
              <a class="popup-img" href="images/portfolio/5.jpg">
                  <div class="custom-card ecommerce">
                           <section class="cards cf">
                           <article class="fancy-card one">
                             <img src="assets/images/1.jpg">
                             <div class="bg-overlay"></div>
                             <div class="v-border"></div>
                             <div class="h-border"></div>
                             <div class="content">
                               <div class="secondary">
                                 <a href="#" class="btn custom-btn2" >View More</a>
                               </div>
                             </div>
                           </article>
                         </section>    
                          <div class="custom-card-body ecommerce-body"><!--  -->
                            <h3 class="product-title">Floral Patterns</h3>
                            <span class="category-name"> Category Name</span>
                            <div class="btn-price">
                              <button class="btn custom-btn"><i class="fas fa-cart-arrow-down"></i> Add to cart </button>
                              <span class="price">$123.89</span>
                            </div>
                          </div>
                        </div><!-- custom-card ends  -->
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-6 portfolio-thumbnail all mockups uikits webdesign">
              <a class="popup-img" href="images/portfolio/6.jpg">
                  <div class="custom-card ecommerce">
                           <section class="cards cf">
                           <article class="fancy-card one">
                             <img src="assets/images/1.jpg">
                             <div class="bg-overlay"></div>
                             <div class="v-border"></div>
                             <div class="h-border"></div>
                             <div class="content">
                               <div class="secondary">
                                 <a href="#" class="btn custom-btn2" >View More</a>
                               </div>
                             </div>
                           </article>
                         </section>    
                          <div class="custom-card-body ecommerce-body"><!--  -->
                            <h3 class="product-title">Floral Patterns</h3>
                            <span class="category-name"> Category Name</span>
                            <div class="btn-price">
                              <button class="btn custom-btn"><i class="fas fa-cart-arrow-down"></i> Add to cart </button>
                              <span class="price">$123.89</span>
                            </div>
                          </div>
                        </div><!-- custom-card ends  -->
              </a>
            </div>

          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- End section portfolio -->
</section>

  <?php /* ?>
<section id="other-service" class="">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="text-center title-wrapper">
          <h3 class="main-title">Designer's Collection</h3>
        </div>
      </div>
    </div>
     <div class="direction-reveal direction-reveal--grid-bootstrap direction-reveal--demo-bootstrap">
    <div class="row justify-content-center">
      <?php for ($x = 0; $x < 3; $x++){  ?>

         <div class="col-lg-3 col-6"><!-- opening or colo -->
           <div class="custom-card ecommerce">
              <section class="cards cf">
              <article class="fancy-card one">
                <img src="assets/images/1.jpg" alt="Image" class="img-fluid">
                <div class="bg-overlay"></div>
                <div class="v-border"></div>
                <div class="h-border"></div>
                <div class="content">
                  <div class="secondary">
                    <h3 class="product-title">Category Name </h3>
                    <a href="#" class="btn custom-btn2" >View More</a>
                  </div>
                </div>
              </article>
            </section>    
           </div><!-- custom-card ends  -->
     </div><!-- closing of col -->
        <?php } ?>
      </div>
    </div>
    </div>
  </div>
</section>
  <?php */ ?>

<!-- custom upload RUGS -->
<section class="block custom-upload-wrapper">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-8">
        <div class="custom-upload">
        <div class="cust-up"></div>
          <div class="row">
            <div class="col-lg-4">
             <figure class="custom-upload-effect">
               <img src="assets/images/rug.png" alt="" />
               <figcaption>
               </figcaption>
               <span class="upload-icon">
                 <img src="assets/images/up2.png"/>
               </span>
               <a href="#"></a>
             </figure>

            </div>
            <div class="col-lg-8">
                <h3 class="product-title">Custom Upload Rugs</h3>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.  </p>
              <button class="btn custom-btn">Upload Now</button>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
</section>

<section class="video-block block">
  <div class="container">
    <div class="row">
      <div class="col-lg-5">
        <div class="video-title">
          <h3 class="title">There is always Your Choice in First Place</h3>
          <span><button class="btn custom-btn2">Read More</button>
        </div>
      </div>
      <div class="col-lg-7">
        <video width="100%"  height="100%" controls>
          <source src="assets/video/bgvid.mp4" type="video/mp4">
          <source src="assets/video/bgvid.mp4" type="video/ogg">
          Your browser does not support the video tag.
        </video>
      </div>
    </div>
  </div>
</section>
<?php include_once('footer.php');  ?>