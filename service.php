<?php include_once('header.php');  ?>
<section class="service-page-wrapper block">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12">
		    <div class="text-center title-wrapper">
		      <h3 class="main-title">Our Services</h3>
		    </div>
		  </div>
		</div>
		<div class="row">
			    <?php for ($x = 0; $x < 3; $x++){  ?>
			<div class="col-lg-4">
				<div class="service-block">
					<div class="img">
						<img src="assets/images/rotate.jpg">
					</div>
					<div class="service-content">
						<h3 class="product-title">Myrug Design </h3>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
					</div>
				</div>
			</div>
			    <?php } ?>
		</div>
	</div>
</section>
<section class="step-rug-wrapper block">
	<div class="container">
			<div class="row">
		  <div class="col-lg-12">
		    <div class="text-center title-wrapper">
		      <h3 class="main-title">Steps TO Make the Rug</h3>
		    </div>
		  </div>
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-1">
				<h3 class="step-number">1</h3>
			</div><!-- close col -->
			<div class="col-lg-9">
				<div class="step-content">
					<h3 class="product-title">PICK YOUR DESIGN</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,</p>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate elei</p>
				</div>
			</div><!-- close col -->
		</div><!-- row close -->
		<div class="row justify-content-center">
			<div class="col-lg-1 order-2">
				<h3 class="step-number">2</h3>
			</div><!-- close col -->
			<div class="col-lg-9 order-1">
				<div class="step-content">
					<h3 class="product-title">PICK YOUR DESIGN</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate elei</p>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,</p>
				</div>
			</div><!-- close col -->
		</div><!-- row close -->
		<div class="row justify-content-center">
			<div class="col-lg-1">
				<h3 class="step-number">3</h3>
			</div><!-- close col -->
			<div class="col-lg-9">
				<div class="step-content">
					<h3 class="product-title">PICK YOUR DESIGN</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,</p>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate elei</p>
				</div>
			</div><!-- close col -->
		</div><!-- row close -->

	</div>
</section>
<?php include_once('footer.php');  ?>