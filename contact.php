<?php include_once('header.php');  ?>
<div class="block">
	<div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-12">
                <div class="contact-block-wrap">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="contact-info">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="icon">
                                              <i class="fa fa-map-marker"></i>
                                        </div>    
                                    </div>
                                    <div class="col-lg-10">
                                        <div class="contct-info-content">
                                            <h3 class="sub-title">Address</h3>
                                            <p>Kalanki, Kathmandu, Nepal</p>
                                        </div>
                                    </div>
                                </div><!-- sub row close -->
                            </div>
                        </div><!-- col close -->
                            <div class="col-lg-4">
                            <div class="contact-info">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="icon">
                                             <i class="fas fa-phone"></i>
                                        </div>    
                                    </div>
                                    <div class="col-lg-10">
                                        <div class="contct-info-content">
                                            <h3 class="sub-title">Telephone</h3>
                                            <p>01-9555-5241</p>
                                        </div>
                                    </div>
                                </div><!-- sub row close -->
                            </div>
                        </div><!-- col close -->
                            <div class="col-lg-4">
                            <div class="contact-info">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="icon">
                                             <i class="far fa-envelope"></i>
                                        </div>    
                                    </div>
                                    <div class="col-lg-10">
                                        <div class="contct-info-content">
                                            <h3 class="sub-title">Email</h3>
                                            <p>contact@myrugdesign.com</p>
                                        </div>
                                    </div>
                                </div><!-- sub row close -->
                            </div>
                        </div><!-- col close -->
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8">
            	 <div class="get-us-wrap ">
                    <div class="get-us text-center">
                        <h1 class="product-title mb-5">Request a quote</h1>
                    </div>
                    <div class="main-conact-form">
                        <form id="contact" method="post" class="form" role="form">
                          <div class="row">
                             <div class="col-xs-12 col-md-12 form-group">
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div><!--  -->
                            <div class="col-xs-6 col-md-6 form-group">
                              <input class="form-control" id="name" name="name" placeholder="Name" type="text" required />
                            </div><!--  -->
                            <div class="col-xs-12 col-md-6 form-group">
                              <input class="form-control" id="email" name="email" placeholder="Email" type="email" required />
                            </div><!--  -->
                            <div class="col-xs-12 col-md-6 form-group">
                              <input class="form-control" id="phone" name="phone" placeholder="Phone Number" type="text" required />
                            </div><!--  -->
                            <div class="col-xs-12 col-md-6 form-group">
                              <select class="form-control" id="styleofrugs">
                                 <option>Style Of Rugs</option>
                                 <option>2</option>
                                 <option>3</option>
                                 <option>4</option>
                                 <option>5</option>
                               </select>    
                            </div><!--  -->
                            <div class="col-xs-12 col-md-6 form-group">
                             <select class="form-control" id="matriels">
                                <option>Matriels</option>
                                <option>option one</option>
                                <option>option two</option>
                                <option>option Three</option>
                                <option>option Four</option>
                              </select> 
                            </div><!--  -->
                            <div class="col-xs-12 col-md-6 form-group">
                                 <div class="row">
                                     <div class="col-lg-8">
                                         <input class="form-control" id="thickness" name="thickness" placeholder="Thickness Number" type="text" required />
                                     </div>
                                     <div class="col-lg-4">
                                         <select class="form-control" id="styleofrugs">
                                            <option>Units</option>
                                            <option>cm</option>
                                            <option>inch</option>
                                            <option>feet</option>
                                            <option>mm</option>
                                          </select> 
                                     </div>
                                 </div>
                            </div><!--  -->
                          <div class="col-xs-12 col-md-12 form-group">
                               <div class="row">
                                   <div class="col-lg-5">
                                       <input class="form-control" id="height" name="height" placeholder="Height" type="text" required />
                                   </div>
                                   <div class="col-lg-5">
                                       <input class="form-control" id="width" name="width" placeholder="Width" type="text" required />
                                   </div>
                                   <div class="col-lg-2">
                                       <select class="form-control" id="styleofrugs">
                                          <option>Units</option>
                                          <option>cm</option>
                                          <option>inch</option>
                                          <option>feet</option>
                                          <option>mm</option>
                                        </select> 
                                   </div>
                               </div>
                          </div><!--  -->
                            <div class="col-xs-12 col-md-12 form-group">
                                <select class="form-control" id="matriels">
                                   <option>Time (GMT)</option>
                                   <option>option one</option>
                                   <option>option two</option>
                                   <option>option Three</option>
                                   <option>option Four</option>
                                 </select> 
                            </div><!--  -->
                            <div class="col-xs-12 col-md-12 form-group">
                              <textarea name="contact-text" cols="5" rows="5" class="form-control" id="contact-text" aria-required="true" aria-invalid="false" placeholder="Your Message"></textarea>
                            </div><!--  -->
                          </div>
                        <br>
                        <button class="btn custom-btn" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>	
        </div>
    </div>
</div>    
<section class="block">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.513583320117!2d85.31070601480977!3d27.701424932359625!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1854cfda3e59%3A0x4191b31405936f34!2sKathmandu+Mall%2C+Sundhara+Marg%2C+Kathmandu+44600!5e0!3m2!1sen!2snp!4v1542466278477" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
<?php include_once('footer.php');  ?>