<!DOCTYPE html>
<html>
<head>
  <title>My Rug Design</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- fonts -->

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,500i,600,700" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  <!-- slider swiper -->
  <link rel="stylesheet" href="assets/css/swiper.min.css" type="text/css" />
   <link rel="stylesheet" href="assets/css/direction-reveal.css" type="text/css" />
     <link rel="stylesheet" type="text/css" href="assets/css/xzoom.css" media="all" />
     <link rel="stylesheet" type="text/css" href="assets/css/magnifier.css">
  <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
  <section class="top-header">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="top-link-wrapper">
            <span class="shipping">Free shipping Worldwide</span>
            <div class="top-social-link">
              <span>Need Help?</span>
              <!--<span>Designers & Architects</span>
              <span>Trade</span>-->
            
            <a href="#" id="cart"><i class="fa fa-shopping-cart"></i> Cart <span class="badge">3</span></a>

                <div class="container shopping-container" style="z-index: 9999; position: absolute; left: 0px; display: none;">
                    <div class="shopping-cart">
                        <div class="shopping-cart-header">
                            <i class="fa fa-shopping-cart cart-icon"></i><span class="badge">3</span>
                            <div class="shopping-cart-total">
                                <span class="lighter-text">Total:</span>
                                <span class="main-color-text">$2,229.97</span>
                            </div>
                        </div> <!--end shopping-cart-header -->

                        <ul class="shopping-cart-items">
                            <li class="clearfix">
                                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/cart-item1.jpg" alt="item1" />
                                <span class="item-name">Sony DSC-RX100M III</span>
                                <span class="item-price">$849.99</span>
                                <span class="item-quantity">Quantity: 01</span>
                            </li>

                            <li class="clearfix">
                                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/cart-item2.jpg" alt="item1" />
                                <span class="item-name">KS Automatic Mechanic...</span>
                                <span class="item-price">$1,249.99</span>
                                <span class="item-quantity">Quantity: 01</span>
                            </li>

                            <li class="clearfix">
                                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/cart-item3.jpg" alt="item1" />
                                <span class="item-name">Kindle, 6" Glare-Free To...</span>
                                <span class="item-price">$129.99</span>
                                <span class="item-quantity">Quantity: 01</span>
                            </li>
                        </ul>

                        <a href="#" class="button">Checkout</a>
                    </div> <!--end shopping-cart -->
                </div> <!--end container -->

         <!--        <span><i class="fas fa-heart"></i>(0)</span> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--  -->

  <section class="middle-bar">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
           <nav class="navbar navbar-expand-lg navbar-dark custom-navbar">
           <!-- <span class="logo">
             <a href=""><img src="assets/images/logo.png"></a>
           </span> -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
             <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="index.php">HOME</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="single-page.php">DESIGNER COLLECTION</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="single-page.php">CLASSIC</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="single-page.php">ELEMENTRY</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="single-page.php">CUSTOM RUG GUIDE</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="single-page.php">ABOUT US</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="contact.php">Contact US</a>
                </li>
              </ul>
              <!--  -->
            </div>
          </nav>
      </div>
    </div>
  </section>
