(function ($) {
    $(document).ready(function () {
        $('.xzoom, .xzoom-gallery').xzoom({zoomWidth: 400, title: true, tint: '#333', smoothZoomMove: 12, Xoffset: 15});
    });
})(jQuery);
$(document).ready(function () {
    /*main slider top*/
    var swiper = new Swiper('.main-slider', {
        slidesPerView: 1,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        autoplay: {
            delay: 5000,
        },
    });
    var swiper = new Swiper('.product-car', {
        slidesPerView: 4,
        loop: true,
        spaceBetween: 30,
        breakpoints: {
            1000: {
                slidesPerView: 2,
                spaceBetween: 30
            }
        },
         breakpoints: {
            576: {
                slidesPerView: 3,
                spaceBetween: 30
            }
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        autoplay: {
            delay: 2500,
        },
    });

    function animationHover(element, animation) {
        element = $(element);
        element.load(
            function () {
                element.addClass('animated ' + animation);
            },
            function () {
                //wait for animation to finish before removing classes
                window.setTimeout(function () {
                    element.removeClass('animated ' + animation);
                }, 2000);
            });
    }

    $('.step-card').each(function () {
        animationHover(this, 'zoomInUp');
    });
  /*  m.attach({
        thumb: '#thumb',
        large: 'http://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Starry_Night_Over_the_Rhone.jpg/400px-Starry_Night_Over_the_Rhone.jpg',
        largeWrapper: 'preview'
    });*/
});
$('#cart').click(function () {
    $('.shopping-container').toggle();
})

// ========================================================================= //
//  Porfolio isotope and filter
// ========================================================================= //


var portfolioIsotope = $('.portfolio-container').isotope({
  itemSelector: '.portfolio-thumbnail',
  layoutMode: 'fitRows'
});

$('#portfolio-flters li').on( 'click', function() {
  $("#portfolio-flters li").removeClass('filter-active');
  $(this).addClass('filter-active');

  portfolioIsotope.isotope({ filter: $(this).data('filter') });
});


// ========================================================================= //