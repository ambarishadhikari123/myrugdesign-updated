<?php include_once('header.php');  ?>
<section class="">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="#">Home</a></li>
				    <li class="breadcrumb-item"><a href="#">Library</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Data</li>
				  </ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<div class="product-display">
					<img class="xzoom img-fluid" src="assets/images/1.jpg" xoriginal="assets/images/1.jpg" />
					<div class="xzoom-thumbs">
					  <a href="assets/images/3.jpg">
					    <img class="xzoom-gallery" width="80" src="assets/images/3.jpg"  xpreview="assets/images/3.jpg">
					  </a>
					  <a href="assets/images/3.jpg">
					    <img class="xzoom-gallery" width="80" src="assets/images/3.jpg">
					  </a>
					  <a href="assets/images/3.jpg">
					    <img class="xzoom-gallery" width="80" src="assets/images/3.jpg">
					  </a>
					  <a href="assets/images/3.jpg">
					    <img class="xzoom-gallery" width="80" src="assets/images/1.jpg">
					  </a>
					</div>
					
				</div>
			</div>
			<div class="col-lg-6">
				<div class="product-detail-wrapper">
					<h1 class="product-title">
						SAFAVIEH · CALIFORNIA SHAG · SG-151 ·
					</h1>
					<div class="products-detail">
						<span class="text">Starting Price: <b>$128</b></span>
						<span class="text">Number of Qantity <b>$128</b></span>
						<span class="text">Discount: <b>$128</b></span>
						<span class="text">Delivery Charge: <b>$128</b></span>
						<span class="text">Starting Price: <b>$128</b></span>
					</div>
					<div class="btn-collection">
						<a href="#"><span class="btn-single"><i class="far fa-heart"></i> add to cart</span></a>
						<a href="#"><span class="btn-single"><i class="fas fa-shopping-bag"></i> Add to Wishlist</span></a>
						<a href="#"><span class="btn-single"><i class="far fa-heart"></i> Order Small Sample</span></a>
					</div>
						<button class="custom-btn btn mt-3"  data-toggle="modal" data-target="#modalQuickView">Customize</button>
						<!-- opening of custom model  -->
						<!-- Modal: modalQuickView -->
						<div class="custom-model-wrapper">
    <div class="modal fade" id="modalQuickView" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-4">
                <!--Carousel Wrapper-->
              <img class="d-block w-100" src="assets/images/1.jpg"
                alt="First slide">
                <!--/.Carousel Wrapper-->
              </div>
              <div class="col-lg-8">
                <form class="wizard-container" method="POST" action="#" id="js-wizard-form">
                    <div class="progress" id="js-progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                             aria-valuemax="100" style="width: 25%;">
                            <span class="progress-val">25%</span>
                        </div>
                    </div>
                    <ul class="nav nav-tab">
                        <li class="">
                            <a href="#tab1" data-toggle="tab">1</a>
                        </li>
                        <li class="active">
                            <a href="#tab2" data-toggle="tab">1</a>
                        </li>
                        <li>
                            <a href="#tab3" data-toggle="tab">1</a>
                        </li>

                        <li>
                            <a href="#tab4" data-toggle="tab">1</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab1">
                            <div class="text-center">
                                <h3>Do you want to recolor the Rug?</h3>
                                <button class="btn-yes step-form-custom btn">Yes</button>
                                <a class="btn-next step-form-custom btn" href="#">No</a>
                            </div>
                            <!--<div class="btn-next-con">
                                <a class="btn-next" href="#">Next</a>
                            </div>-->
                        </div>
                        <div class="tab-pane active" id="tab2">
                            <div class="text-center">
                                <h3>Do you want to customize the size of your Rug?</h3>
                                <button class="btn-yes step-form-custom btn">Yes</button>
                                <a class="btn-next step-form-custom btn" href="#">No</a>
                            </div>
                            <div class="custom-model-detail">
                                <p class="custom-model-para">Customize the size</p>
                                <div class="row justify-content-center">
                                    <label for="height" class="col-2">Height</label>
                                    <div class="col-4">
                                        <input type="text" id="height" class="form-control" placeholder="">
                                    </div>

                                    <label for="width" class="col-2">Width</label>
                                    <div class="col-4">
                                        <input type="text" id="width" class="form-control" placeholder="">
                                    </div>

                                    <label for="unit" class="col-2">Unit</label>
                                    <div class="col-6">
                                        <select name="unit" id="unit" class="form-control">
                                            <option value="">Select Unit</option>
                                            <option value="1">mm</option>
                                            <option value="1">cm</option>
                                            <option value="1">in</option>
                                            <option value="1">ft</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-next-con">
                                <a class="btn-back" href="#">back</a>
                                <!--  <a class="btn-next" href="#">Next</a>-->
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <div class="text-center">
                                <h3>Do you want to change the thickness?</h3>
                                <button class="btn-yes step-form-custom btn">Yes</button>
                                <a class="btn-next step-form-custom btn" href="#">No</a>
                            </div>
                            <div class="custom-model-detail">
                                <p class="custom-model-para">How thick do you want your Rug to be?</p>
                                <div class="row">

                                    <label for="thickness" class="col-2">Thickness</label>
                                    <div class="col-4">
                                        <select name="thickness" id="thickness" class="form-control">
                                            <option value="">Select Unit</option>
                                            <option value="1">mm</option>
                                            <option value="1">cm</option>
                                            <option value="1">in</option>
                                            <option value="1">ft</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-next-con">
                                <a class="btn-back" href="#">back</a>
                                <!--<a class="btn-next" href="#">Next</a>-->
                            </div>
                        </div>

                        <div class="tab-pane" id="tab4">
                            <div class="text-center">
                                <h3>Do you want to customize the yarn?</h3>
                                <button class="btn-yes step-form-custom btn">Yes</button>
                                <a class="btn-next step-form-custom btn" href="#">No</a>
                            </div>
                            <div class="custom-model-detail">
                                <p class="custom-model-para">What yarn do you want to use?</p>
                                <div class="row">

                                    <label for="yarn" class="col-2">Yarn</label>
                                    <div class="col-4">
                                        <select name="yarn" id="yarn" class="form-control">
                                            <option value="">Select Unit</option>
                                            <option value="1">mm</option>
                                            <option value="1">cm</option>
                                            <option value="1">in</option>
                                            <option value="1">ft</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-next-con">
                                <a class="btn-back" href="#">back</a>
                                <a class="btn-last" href="#">Submit</a>
                            </div>
                        </div>
                    </div>
                </form>
              </div><!-- close of column -->
          </div>
        </div>
      </div>
    </div>
    </div>
</div>
						<!-- opening of custom model  -->
					<div class="accordion-wrapper">
						<div id="accordion">
							  <div class="accordion-single">
							    <div class="" id="headingOne">
							      <h5 class="mb-0">
							        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							          Rug Detail
							        </button>
							      </h5>
							    </div>
							    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							      <div class="card-body">
							        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. 
							      </div>
							    </div>
							  </div>
							  <div class="accordion-single">
							    <div class="" id="headingTwo">
							      <h5 class="mb-0">
							        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							          Description
							        </button>
							      </h5>
							    </div>
							    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							      <div class="card-body">
							        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. 
							      </div>
							    </div>
							  </div>
							  <div class="accordion-single">
							    <div class="" id="headingThree">
							      <h5 class="mb-0">
							        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							          Payement Method
							        </button>
							      </h5>
							    </div>
							    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							      <div class="card-body">
							        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. 
							      </div>
							    </div>
							  </div>
</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

<section id="" class="mb-">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="main-title-wrapper">
          <h3 class="main-title">Rugs by Style</h3>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
      <?php for ($x = 0; $x < 5; $x++){  ?>
      <div class="col-lg-4">
     <section class="cards cf">
          <article class="fancy-card one">
            <img src="assets/images/1.jpg">
            <div class="bg-overlay"></div>
            <div class="v-border"></div>
            <div class="h-border"></div>
            <div class="content">
              <div class="primary">
                <h2 class="f-22">Contemporary</h2>
              </div>
              <div class="secondary">
                <h3 class="f-22">Contemporary</h3>
                <a href="#" class="button" >View More</a>
              </div>
            </div>
          </article>
        </section>
      </div>
      <?php } ?>
    </div>
  </div>
</section>
<?php include_once('footer.php');  ?>