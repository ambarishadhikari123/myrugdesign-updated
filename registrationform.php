<?php include_once('header.php');  ?>
<br>
<br>
<section class="block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9">
					<div class="contact-form">
						<div class="custom-card">
							<img src="assets/images/11oct-black.png">
							<h3>Registration form</h3>
						</div>
					<form id="contact" method="post" class="form" role="form">
					  <div class="row">
					    <div class="col-xs-12 col-md-12 form-group">
					      <input class="form-control" id="name" name="name" placeholder="Name" type="text" required />
					    </div><!--  -->
					    <div class="col-xs-12 col-md-6 form-group">
					      <input class="form-control" id="email" name="email" placeholder="Email" type="email" required />
					    </div><!--  -->
					    <div class="col-xs-12 col-md-6 form-group">
					      <input class="form-control" id="phone" name="phone" placeholder="Phone Number" type="text" required />
					    </div><!--  -->
					    <div class="col-xs-12 col-md-6 form-group">
					      <input class="form-control" id="Username" name="username" placeholder="Username" type="text" required />
					    </div><!--  -->
					    <div class="col-xs-12 col-md-6 form-group">
					      <input class="form-control" id="password" name="subject" placeholder="Password" type="Password" required />
					    </div><!--  -->
					    <div class="col-xs-12 col-md-6 form-group">
					      <input class="form-control" id="address1" name="address1" placeholder="Perment Address" type="text" required />
					    </div><!--  -->
					    <div class="col-xs-12 col-md-6 form-group">
					      <input class="form-control" id="address2" name="address2" placeholder="Temporary Address" type="text" required />
					    </div><!--  -->
					   	<div class="col-xs-12 col-md-4 form-group">
					      <select name="your-Country" class="form-control" aria-required="true" aria-invalid="false">
					      			<option value="">Select Country</option>
					      			<option value="Kansas">Afghanistan</option>
					      			<option value="Kentucky">Albania</option>
					      			<option value="Louisiana">Algeria</option>
					      			<option value="Maine">Angola</option>
					      			<option value="Kentucky">Albania</option>
					      			<option value="Louisiana">Algeria</option>
					      			<option value="Maine">Angola</option>
					      			<option value="Kentucky">Albania</option>
					      			<option value="Louisiana">Algeria</option>
					      			<option value="Maine">Angola</option>
					      		</select>
					    </div><!--  -->
					    <div class="col-xs-12 col-md-4 form-group">
					      <select name="your-state" class="form-control" aria-required="true" aria-invalid="false">
					      			<option value="">Select State</option>
					      			<option value="Kansas">Kansas</option>
					      			<option value="Kentucky">Kentucky</option>
					      			<option value="Louisiana">Louisiana</option>
					      			<option value="Maine">Maine</option>
					      			<option value="Maryland">Maryland</option>
					      			<option value="Rhode Island">Rhode Island</option>
					      			<option value="South Carolina">South Carolina</option>
					      			<option value="South Dakota">South Dakota</option>
					      			<option value="Tennessee">Tennessee</option>
					      			<option value="Texas">Texas</option>
					      		</select>
					    </div><!--  -->
					    <div class="col-xs-12 col-md-4 form-group">
					      <input class="form-control" id="phone" name="City" placeholder="City" type="text" required />
					    </div><!--  -->
					  </div>
					<br>
					<button class="btn custom-btn" type="submit">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<br>
<br>
<?php include_once('footer.php');  ?>