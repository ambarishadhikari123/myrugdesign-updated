<?php include_once('header.php');  ?>
<!-- <section class="block">
	<p>This is the section for user dashboard nav bar</p>
</section> -->
<section class="user-dashboard">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-3"><!-- left side bar -->
				<div class="user-leftbar">
					<div class="user-dec">
						<img src="assets/images/pp.jpg">
						<p class="name">Ambarish Adhikari</p>
						<p>aambarishadhikari@gmail.com</p>
					</div>
					<div class="links">
						<p><a href="">&raquo  My orders</a></p>
						<p><a href="">&raquo  Custom Rugs Upload</a></p>
						<p><a href="">&raquo  Payment History</a></p>
						<p><a href="">&raquo  Wishlist</a></p>
						<p><a href="">&raquo  Settings</a></p>
						<p><a href=""><b> Log Out </b></a></p>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="user-rightbar">
					<!-- just a try -->
					<div class="table-responsive">
					          <table class="table">
					            <thead>
					            	
					            	<!--  -->
					              <tr>
					                <th>Product Name</th>
					                <th class="text-center">Quantity</th>
					                <th class="text-center">Subtotal</th>
					                
					                <th class="text-center"><a class="btn btn-sm btn-outline-danger" href="#">Clear Cart</a></th>
					              </tr>
					            </thead>
					            <tbody>
					            	<?php  for ($i=0; $i < 4 ; $i++) {  ?>
					              <tr class="cust-table">
					                <td>
					                  <div class="product-item">
					                  	<a class="product-thumb" href="#">
					                  		<img src="assets/images/1.jpg" alt="Product"></a>
					                    <div class="product-info">
					                      <h4 class="product-title">
					                      	<a href="#">Unionbay Park</a></h4><span><em>Size:</em> 10.5</span><span> &nbsp &nbsp<em>Color:</em> Dark Blue</span>
					                    </div>
					                  </div>
					                </td>
					                <td class="text-center">
					                  <div class="count-input">
					                    <select class="form-control">
					                      <option>1</option>
					                      <option>2</option>
					                      <option>3</option>
					                      <option>4</option>
					                      <option>5</option>
					                    </select>
					                  </div>
					                </td>
					                <td class="text-center text-lg text-medium">$43.90</td>
					               
					                <td class="text-center">
					                	<a class="remove-from-cart" href="#" >
					                		<button class="btn custom-btn">View More</button>
					                	</a>
					                </td>
					              </tr>
					                        <?php } ?>
					              <tr>
					              	<!--  -->
					            </tbody>
					          </table>
					        </div>

					<!-- just a try -->
				</div>
			</div>
		</div>
	</div>
</section>

<?php include_once('footer.php');  ?>