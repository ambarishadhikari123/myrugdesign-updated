<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-6">
				<div class="footer-about">
					<h3 class="title">About Us</h3>
					<p class="mb-4">My Rug Design Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
					<p>Phone: +019955442236</p>
					<p>Email: support@myrugdesign.com</p>
				</div>
			</div>
			<!--  -->
			<div class="col-lg-3 col-6">
				<div class="footer-about link">
					<h3 class="title">Account</h3>
					<p><a href="">&raquo Your Account</p></a>
					<p><a href="">&raquo Free Shipping Policy</p></a>
					<p><a href="">&raquo Your Cart</p></a>
					<p><a href="">&raquo Return Policy</p></a>
					<p><a href="">&raquo Delivery Info</p></a>
				</div>
			</div>
			<div class="col-lg-3 col-6">
				<div class="footer-about link">
					<h3 class="title">Support</h3>
					<p><a href="">&raquo Help</p></a>
					<p><a href="">&raquo Product Support</p></a>
					<p><a href="">&raquo Terms & Condtion</p></a>
					<p><a href="">&raquo Payment Method</p></a>
					<p><a href="">&raquo Affiliate Programs</p></a>
				</div>
			</div>
			<div class="col-lg-3 col-6">
				<div class="footer-about link">
					<h3 class="title">Join Our Mailing </h3>
					<p><a href="">&raquo Help</p></a>
					<p><a href="">&raquo Product Support</p></a>
					<p><a href="">&raquo Terms & Condtion</p></a>
					<p><a href="">&raquo Payment Method</p></a>
					<p><a href="">&raquo Affiliate Programs</p></a>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- cnds -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!-- swiper js -->
<script type="text/javascript" src="assets/js/swiper.min.js"></script>
<script type="text/javascript" src="assets/js/bundle.js"></script>
 <script type="text/javascript" src="assets/js/xzoom.min.js"></script>
 <script type="text/javascript" src="assets/js/Event.js"></script>
 <script type="text/javascript" src="assets/js/Magnifier.js"></script>
 <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
 <script type="text/javascript" src="assets/js/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="assets/js/global.js"></script>
<script src="lib/isotope/isotope.pkgd.min.js"></script>
 <!--<script type="text/javascript">
 var evt = new Event(),
     m = new Magnifier(evt);
 </script>-->
<script src="assets/js/custom.js"></script>
</body>
</html>